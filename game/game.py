import time
import asyncio

class Clock(object):
	def __init__(self):
		self.rate = 30
		self.interval = 1./self.rate
		self.lastTick = time.time()

	async def tick(self):
		currentTime = time.time()
		dt = currentTime - self.lastTick

		sleepTime = self.interval - dt
		if sleepTime > 0:
			await asyncio.sleep(sleepTime)

			currentTime = time.time()
			dt = currentTime - self.lastTick

		self.lastTick = currentTime

		return dt

class Game(object):
	def __init__(self):
		self.clock = Clock()

	def update(self, dt):
		pass#print(dt)

	async def loop(self):
		while True:
			dt = await self.clock.tick()
			self.update(dt)