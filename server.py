from game import Game

from sanic import Sanic
from sanic.response import html

import socketio
import asyncio
import uvloop

sio = socketio.AsyncServer(async_mode='sanic')
app = Sanic(__name__)
sio.attach(app)

game = Game()

@app.route('/')
async def index(request):
	with open('index.html') as f:
		return html(f.read())

app.static('/static', './client')

@sio.on('connect')
async def connect(sid, environ):
	await sio.emit('socketID', { id: sid })

@sio.on('login')
def login(sid, data):
	pass

@sio.on('register')
def register(sid, data):
	pass

@sio.on('disconnect')
def disconnect(sid):
	print('disconnect', sid)

server = app.create_server(host="0.0.0.0", port=8000)

asyncio.set_event_loop(uvloop.new_event_loop())
loop = asyncio.get_event_loop()

asyncio.ensure_future(game.loop())
asyncio.ensure_future(server)

try:
	loop.run_forever()
except:
	loop.stop()