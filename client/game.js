define(['gui'], (gui)=>{

class Game {
	construct(socket, renderer) {
		this.socket = socket;
		this.renderer = renderer;
	}

	draw() {}
}

return Game

});