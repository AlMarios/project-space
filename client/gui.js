define([], () => {

const app = new Vue({
	el: '#gui',
	data: {
		loginOpened: true,
		registerOpened: false,
		credentials: {
			username: '',
			password: ''
		},
		register: {
			username: '',
			password: '',
			repeatPassword: ''
		}
	},
	methods: {
		Login() {
			let {username, password} = this.credentials;
			console.log(username, password);
		},
		Regiser() {
			let {username, password, repeatPassword} = this.regiser;
			console.log(username, password, repeatPassword);
		},
		openRegister() {
			this.loginOpened = false;
			this.registerOpened = true;
		},
		openLogin() {
			this.loginOpened = true;
			this.registerOpened = false;
		}
	}
});

return app;

});